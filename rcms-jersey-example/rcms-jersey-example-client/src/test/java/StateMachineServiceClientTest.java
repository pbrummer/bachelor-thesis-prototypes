import cern.cms.rcms_jersey_example.statemachine.Input;

public class StateMachineServiceClientTest {

    public static void main(String[] args) {
        StateMachineServiceClient stateMachineServiceClient =
                new StateMachineServiceClient(
                        "http://localhost:8080/rcms-jersey-example-service/webapi/state-machine"
                );

        System.out.println(
                stateMachineServiceClient.getState("sm1").getName()
        );
        System.out.println(
                stateMachineServiceClient.getState("sm2").getName()
        );

        stateMachineServiceClient.processInput("sm1",
                new Input("Start"));
        stateMachineServiceClient.processInput("sm2",
                new Input("Start"));

        System.out.println(
                stateMachineServiceClient.getState("sm1").getName()
        );
        System.out.println(
                stateMachineServiceClient.getState("sm2").getName()
        );
    }

}
