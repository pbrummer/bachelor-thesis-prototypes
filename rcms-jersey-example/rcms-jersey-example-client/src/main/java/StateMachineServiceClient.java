import cern.cms.rcms_jersey_example.api.StateMachineResource;
import cern.cms.rcms_jersey_example.statemachine.Input;
import cern.cms.rcms_jersey_example.statemachine.State;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;

public class StateMachineServiceClient implements StateMachineResource {

    private final String restEndpoint;
    private final Client client;

    public StateMachineServiceClient(String restEndpoint) {
        this.restEndpoint = restEndpoint;
        this.client = ClientBuilder.newClient();
    }

    @Override
    public State getState(String stateMachine) {
        return this.client
                .target(this.restEndpoint)
                .path("state")
                .path(stateMachine)
                .request(MediaType.APPLICATION_JSON)
                .get(State.class);
    }

    @Override
    public void processInput(String stateMachine, Input input) {
        this.client
                .target(this.restEndpoint)
                .path(stateMachine)
                .request(MediaType.APPLICATION_JSON)
                .post(Entity.entity(input, MediaType.APPLICATION_JSON));
    }
}
