package cern.cms.rcms_jersey_example;

import cern.cms.rcms_jersey_example.statemachine.State;
import cern.cms.rcms_jersey_example.statemachine.StateMachine;
import cern.cms.rcms_jersey_example.statemachine.States;
import cern.cms.rcms_jersey_example.statemachine.StateMachineRepository;
import org.glassfish.jersey.server.ResourceConfig;

import java.util.HashMap;
import java.util.Map;

public class StateMachineService extends ResourceConfig {

    public StateMachineService() {

        Map<String, State> stateMachine1Transitions = new HashMap<>();
        stateMachine1Transitions.put("Configure", States.ReadyState);
        stateMachine1Transitions.put("Start", States.RunningState);
        stateMachine1Transitions.put("Stop", States.ReadyState);

        StateMachine stateMachine1 = new StateMachine(States.InitialState,
                stateMachine1Transitions);

        Map<String, State> stateMachine2Transitions = new HashMap<>();
        stateMachine2Transitions.put("Configure", States.UndefinedState);
        stateMachine2Transitions.put("Start", States.ReadyState);
        stateMachine2Transitions.put("Stop", States.InitialState);

        StateMachine stateMachine2 = new StateMachine(States.InitialState,
                stateMachine2Transitions);

        StateMachineRepository repository = new StateMachineRepository();
        repository.addStateMachine("sm1", stateMachine1);
        repository.addStateMachine("sm2", stateMachine2);

        DependencyBinder dependencyBinder = new DependencyBinder(repository);
        register(dependencyBinder);

        packages(true, "cern.cms.rcms_jersey_example.api");
    }

}
