package cern.cms.rcms_jersey_example.api;

import cern.cms.rcms_jersey_example.statemachine.Input;
import cern.cms.rcms_jersey_example.statemachine.State;
import cern.cms.rcms_jersey_example.statemachine.StateMachine;
import cern.cms.rcms_jersey_example.statemachine.StateMachineRepository;

import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("state-machine")
public class StateMachineResourceImpl implements StateMachineResource {

    private StateMachineRepository repository;

    @Inject
    public StateMachineResourceImpl(StateMachineRepository repository) {
        this.repository = repository;
    }

    @Override
    @Path("state/{sm}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public State getState(@NotNull @PathParam(value="sm") String stateMachine) {
        StateMachine sm = this.repository.getStateMachine(stateMachine);
        if (sm == null) {
            return null;
        } else {
            return sm.getCurrentState();
        }
    }

    @Override
    @POST
    @Path("{sm}")
    @Consumes(MediaType.APPLICATION_JSON)
    public void processInput(@NotNull @PathParam(value="sm") String stateMachine,
                                 @NotNull Input input) {
        StateMachine sm = this.repository.getStateMachine(stateMachine);
        if (sm == null) {
            return;
        } else {
            sm.transition(input);
        }
    }
}
