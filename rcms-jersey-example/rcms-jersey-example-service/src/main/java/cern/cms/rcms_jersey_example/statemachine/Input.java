package cern.cms.rcms_jersey_example.statemachine;

import java.util.HashSet;
import java.util.Set;

public class Input {

    private String name;

    private Set<InputParameter> parameters;

    public Input(String name, Set<InputParameter> parameters) {
        this.name = name;
        this.parameters = parameters;
    }

    public Input(String name) {
        this(name, new HashSet<InputParameter>(0));
    }

    private Input() {}

    public String getName() {
        return name;
    }

    public Set<InputParameter> getParameters() {
        return parameters;
    }

    public void setParameters(Set<InputParameter> parameters) {
        this.parameters = parameters;
    }

    public static class InputParameter {

        private String name;

        private String value;

        public InputParameter(String name, String value) {
            this.name = name;
            this.value = value;
        }

        private InputParameter() {}

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

}
