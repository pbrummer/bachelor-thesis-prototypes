package cern.cms.rcms_jersey_example.statemachine;

public class State {

    private String name;

    public State(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private State() {}

}
