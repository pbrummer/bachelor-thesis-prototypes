package cern.cms.rcms_jersey_example.statemachine;

import java.util.HashMap;
import java.util.Map;

public class StateMachineRepository {

    private Map<String, StateMachine> stateMachines;

    public StateMachineRepository() {
        this.stateMachines = new HashMap<>();
    }

    public void addStateMachine(String name, StateMachine stateMachine) {
        this.stateMachines.put(name, stateMachine);
    }

    public StateMachine getStateMachine(String name) {
        return this.stateMachines.get(name);
    }

}
