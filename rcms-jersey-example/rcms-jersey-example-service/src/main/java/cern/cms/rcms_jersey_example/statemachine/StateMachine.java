package cern.cms.rcms_jersey_example.statemachine;

import java.util.Map;

public class StateMachine {

    private State currentState;

    private Map<String, State> transitions;

    public StateMachine(State initialState, Map<String, State> transitions) {
        this.currentState = initialState;
        this.transitions = transitions;
    }

    public State getCurrentState() {
        return this.currentState;
    }

    public void transition(Input input) {
        State targetState = this.transitions.get(input.getName());
        if (targetState == null) {
            targetState = States.UndefinedState;
        }
        this.currentState = targetState;
    }

}
