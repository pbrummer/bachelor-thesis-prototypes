package cern.cms.rcms_jersey_example.statemachine;

public class States {

    public static final State UndefinedState = new State("Undefined");
    public static final State InitialState = new State("Initial");
    public static final State ReadyState = new State("Ready");
    public static final State RunningState = new State("Running");

}
