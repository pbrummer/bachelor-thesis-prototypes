package cern.cms.rcms_jersey_example;

import cern.cms.rcms_jersey_example.statemachine.StateMachineRepository;
import org.glassfish.hk2.utilities.binding.AbstractBinder;

public class DependencyBinder extends AbstractBinder {

    private StateMachineRepository stateMachineRepository;

    public DependencyBinder(StateMachineRepository stateMachineRepository) {
        this.stateMachineRepository = stateMachineRepository;
    }

    @Override
    protected void configure() {
        bind(this.stateMachineRepository).to(StateMachineRepository.class);
    }
}

