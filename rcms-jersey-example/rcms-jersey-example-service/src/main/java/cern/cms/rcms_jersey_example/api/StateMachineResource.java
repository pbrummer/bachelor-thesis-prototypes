package cern.cms.rcms_jersey_example.api;

import cern.cms.rcms_jersey_example.statemachine.Input;
import cern.cms.rcms_jersey_example.statemachine.State;

import javax.validation.constraints.NotNull;

public interface StateMachineResource {

    State getState(@NotNull String stateMachine);

    void processInput(@NotNull String stateMachine,
                      @NotNull Input input);

}