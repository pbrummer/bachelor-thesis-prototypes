package axis2.service;

import java.util.HashSet;
import java.util.Set;

public class Input {

    private final String name;

    private Set<InputParameter> parameters;

    public Input(String name, Set<InputParameter> parameters) {
        this.name = name;
        this.parameters = parameters;
    }

    public Input(String name) {
        this(name, new HashSet<>(0));
    }

    public String getName() {
        return name;
    }

    public Set<InputParameter> getParameters() {
        return parameters;
    }

    public void setParameters(Set<InputParameter> parameters) {
        this.parameters = parameters;
    }

    public static class InputParameter {

        private String name;

        private String value;

        public InputParameter(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }

}
