package axis2.service;

public class State {

    private final String name;

    public State(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
