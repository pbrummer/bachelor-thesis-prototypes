package axis2.service;

import java.util.HashMap;
import java.util.Map;

public class StateMachineServiceImpl implements StateMachineService {

    private static final State UndefinedState = new State("Undefined");
    private static final State InitialState = new State("Initial");
    private static final State ReadyState = new State("Ready");
    private static final State RunningState = new State("Running");

    private State state = InitialState;

    private static Map<String, State> transitions = new HashMap<>();

    static {
        transitions.put("Configure", ReadyState);
        transitions.put("Start", RunningState);
        transitions.put("Stop", ReadyState);
    }

    @Override
    public State getState() {
        return this.state;
    }

    @Override
    public void processInput(Input input) {
        State targetState = transitions.get(input.getName());
        if (targetState == null) {
            targetState = UndefinedState;
        }
        this.state = targetState;
    }

}
