package axis2.service;

public interface StateMachineService {

    State getState();

    void processInput(Input input);

}
