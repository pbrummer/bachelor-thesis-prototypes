package axis2.client;

public class TestClient {

    public static void main(String[] args) throws Exception {
        StateMachineServiceStub serviceStub = new StateMachineServiceStub();

        StateMachineServiceStub.GetState getState = new StateMachineServiceStub.GetState();
        System.out.println("Current state: " + serviceStub.getState(getState).get_return());

        System.out.println("Sending Start Input");
        StateMachineServiceStub.ProcessInput processInput = new StateMachineServiceStub.ProcessInput();
        processInput.setArgs0("Start");
        serviceStub.processInput(processInput);

        System.out.println("Current state: " + serviceStub.getState(getState).get_return());

        System.out.println("Sending Stop Input");
        processInput.setArgs0("Stop");
        serviceStub.processInput(processInput);

        System.out.println("Current state: " + serviceStub.getState(getState).get_return());
    }

}
