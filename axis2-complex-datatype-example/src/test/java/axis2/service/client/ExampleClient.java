package axis2.service.client;

import javax.xml.namespace.QName;
import java.net.URL;

public class ExampleClient {

    public static void main(String[] args) throws Exception {
        URL endpoint = new URL("http://localhost:8080/axis2/services/StateMachineService");
        QName qname = new QName("http://service.axis2/","StateMachineService");

        StateMachineService_Service service = new StateMachineService_Service(endpoint, qname);
        StateMachineService serviceImpl = service.getStateMachineServiceImplPort();

        System.out.println(serviceImpl.getState().getName());

        Input stopInput = new Input();
        stopInput.setName("Stop");

        serviceImpl.processInput(stopInput);
        System.out.println(serviceImpl.getState().getName());

        Input startInput = new Input();
        startInput.setName("Start");

        InputParameter param1 = new InputParameter();
        param1.setName("param1");
        param1.setValue("value1");
        startInput.getParameters().add(param1);

        serviceImpl.processInput(startInput);
        System.out.println(serviceImpl.getState().getName());
    }

}
