package axis2.service;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService
public interface StateMachineService {

    @WebMethod(operationName = "getState")
    @WebResult(targetNamespace = "http://service.axis2/", name = "state")
    @RequestWrapper(localName = "getState", className = "axis2.service.jaxws.GetState")
    @ResponseWrapper(localName = "getStateResponse", className = "axis2.service.jaxws.GetStateResponse")
    State getState();

    @WebMethod(operationName = "processInput")
    @RequestWrapper(localName = "processInput", className = "axis2.service.jaxws.ProcessInput")
    @ResponseWrapper(localName = "processInputResponse", className = "axis2.service.jaxws.ProcessInputResponse")
    void processInput(
            @WebParam(name = "input", targetNamespace = "http://service.axis2/")
                    Input input
    );

}
