package axis2.service;

import javax.jws.WebService;
import java.util.HashMap;
import java.util.Map;

@WebService(endpointInterface = "axis2.service.StateMachineService",
            serviceName = "StateMachineService",
            wsdlLocation = "META-INF/StateMachineService.wsdl")
public class StateMachineServiceImpl implements StateMachineService {

    private static final State UndefinedState = new State("Undefined");
    private static final State InitialState = new State("Initial");
    private static final State ReadyState = new State("Ready");
    private static final State RunningState = new State("Running");

    private static State state = InitialState;

    private static Map<String, State> TRANSITIONS = new HashMap<>();

    static {
        TRANSITIONS.put("Configure", ReadyState);
        TRANSITIONS.put("Start", RunningState);
        TRANSITIONS.put("Stop", ReadyState);
    }

    @Override
    public State getState() {
        return this.state;
    }

    @Override
    public void processInput(Input input) {
        State targetState = TRANSITIONS.get(input.getName());
        if (targetState == null) {
            targetState = UndefinedState;
        }
        state = targetState;
    }

}