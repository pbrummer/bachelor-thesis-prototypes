
package axis2.service.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the axis2.service.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProcessInput_QNAME = new QName("http://service.axis2/", "processInput");
    private final static QName _ProcessInputResponse_QNAME = new QName("http://service.axis2/", "processInputResponse");
    private final static QName _GetStateResponse_QNAME = new QName("http://service.axis2/", "getStateResponse");
    private final static QName _GetState_QNAME = new QName("http://service.axis2/", "getState");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: axis2.service.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetState }
     * 
     */
    public GetState createGetState() {
        return new GetState();
    }

    /**
     * Create an instance of {@link GetStateResponse }
     * 
     */
    public GetStateResponse createGetStateResponse() {
        return new GetStateResponse();
    }

    /**
     * Create an instance of {@link ProcessInput }
     * 
     */
    public ProcessInput createProcessInput() {
        return new ProcessInput();
    }

    /**
     * Create an instance of {@link ProcessInputResponse }
     * 
     */
    public ProcessInputResponse createProcessInputResponse() {
        return new ProcessInputResponse();
    }

    /**
     * Create an instance of {@link Input }
     * 
     */
    public Input createInput() {
        return new Input();
    }

    /**
     * Create an instance of {@link InputParameter }
     * 
     */
    public InputParameter createInputParameter() {
        return new InputParameter();
    }

    /**
     * Create an instance of {@link State }
     * 
     */
    public State createState() {
        return new State();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessInput }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.axis2/", name = "processInput")
    public JAXBElement<ProcessInput> createProcessInput(ProcessInput value) {
        return new JAXBElement<ProcessInput>(_ProcessInput_QNAME, ProcessInput.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessInputResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.axis2/", name = "processInputResponse")
    public JAXBElement<ProcessInputResponse> createProcessInputResponse(ProcessInputResponse value) {
        return new JAXBElement<ProcessInputResponse>(_ProcessInputResponse_QNAME, ProcessInputResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetStateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.axis2/", name = "getStateResponse")
    public JAXBElement<GetStateResponse> createGetStateResponse(GetStateResponse value) {
        return new JAXBElement<GetStateResponse>(_GetStateResponse_QNAME, GetStateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetState }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.axis2/", name = "getState")
    public JAXBElement<GetState> createGetState(GetState value) {
        return new JAXBElement<GetState>(_GetState_QNAME, GetState.class, null, value);
    }

}
