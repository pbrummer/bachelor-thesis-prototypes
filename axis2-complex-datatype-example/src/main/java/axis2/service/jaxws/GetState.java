
package axis2.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getState", namespace = "http://service.axis2/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getState", namespace = "http://service.axis2/")
public class GetState {


}
