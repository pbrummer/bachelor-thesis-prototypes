
package axis2.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "processInput", namespace = "http://service.axis2/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processInput", namespace = "http://service.axis2/")
public class ProcessInput {

    @XmlElement(name = "input", namespace = "http://service.axis2/")
    private axis2.service.Input input;

    /**
     * 
     * @return
     *     returns Input
     */
    public axis2.service.Input getInput() {
        return this.input;
    }

    /**
     * 
     * @param input
     *     the value for the input property
     */
    public void setInput(axis2.service.Input input) {
        this.input = input;
    }

}
