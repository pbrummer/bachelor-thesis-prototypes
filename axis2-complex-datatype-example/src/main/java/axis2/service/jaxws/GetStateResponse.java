
package axis2.service.jaxws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "getStateResponse", namespace = "http://service.axis2/")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStateResponse", namespace = "http://service.axis2/")
public class GetStateResponse {

    @XmlElement(name = "state", namespace = "http://service.axis2/")
    private axis2.service.State state;

    /**
     * 
     * @return
     *     returns State
     */
    public axis2.service.State getState() {
        return this.state;
    }

    /**
     * 
     * @param state
     *     the value for the state property
     */
    public void setState(axis2.service.State state) {
        this.state = state;
    }

}
