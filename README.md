# Run Control Framework Prototype Instructions

All prototypes come with IntelliJ IDEA (Axis2, Jersey, Spring) or WebStorm (Angular) project files. The IntelliJ IDEA Ultimate edition might be required for Java EE components to work.

## Installing Tomcat

- Download a 8.5.x version of tomcat from: https://tomcat.apache.org/download-80.cgi
- Unpack the archive and run Tomcat using the `bin/startup.sh` script.
- Alternatively, Tomcat can be started with jpda using `bin/catalina.sh jpda start`, making it possible to attach a debugger on localhost port 8000.

## Axis2

The Axis2 prototypes require a Tomcat installation as well as Ant and Maven.

### Installing Axis2

Axis2 can be downloaded from: http://axis.apache.org/axis2/java/core/download.html
The axis2.war contained in the WAR distribution needs to be copied into the Tomcat installation's `webapps` folder.

### Compiling and deploying the axis2-example project

The project can be compiled and packaged by running `ant generate.service`, producing a .aar File, which then needs to be copied to the `webapps/axis2/WEB-INF/services` directory of the local Tomcat installation.

The axis-example-client can be run directly from its sub-directory, for example by using an IDE or by running `ant compile.src` followed by `java -classpath build/classes axis2.client.TestClient`.

The axis2-complex-datatype-aar-example prototype can be compiled and deployed in the same way.

### Compiling and deploying the axis2-complex-datatype-example project

The project can be compiled and packaged by running `mvn package`, producing a .jar File, which then needs to be copied to the `webapps/axis2/WEB-INF/servicejars` directory of the local Tomcat installation. The `servicejars` directory might not exist, in which case it needs to be created first.

The client can be run using and IDE or `java -cp target/test-classes;target/classes axis2.service.client.ExampleClient`.

The axis2-jaxws-example prototype can be compiled and deployed in the same way.

## Jersey

The Jersey prototype requires a Tomcat installation as well as Maven.

### Compiling and deploying the web service

The service can be compiled by running `mvn package` in the `rcms-jersey-example-service` folder, copying the resulting `rcms-jersey-example-service.war` into the Tomcat installation's `webapps` folder.

### Compiling and running the test client

The service can be compiled by running `mvn compile` in the `rcms-jersey-example-client` folder, then running `java -cp target/test-classes;target/classes StateMachineServiceClientTest`.

## Spring

The Spring prototype is compiled using Gradle. The Gradle wrapper is shipped with the project, making a separate installation unnecessary.

### Starting the Spring Boot application

The application can be run using `./gradlew bootRun` or `gradle bootRun`, if Gradle is installed. This will start the integrated Tomcat server on localhost and port 8080, potentially conflicting with local Tomcat installations. Running Spring Boot will also execute the local test client.

## Angular

The Angular prototype requires an installation of Node.js and npm to be built. Since angular-cli was used to generate the project, you can find more information regarding the build process in the documentation provided with the project.

### Installing the requirements

The project requirements can be installed by running

    npm install

### Compiling and deploying the application

The Angular application can be compiled and deployed by running

    ng serve --open
    
or

    npm start -- --open
    
which will open a web browser window and load the Angular application.
