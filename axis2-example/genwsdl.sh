export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk.x86_64
export AXIS_HOME=/home/pbrummer/axis2-1.7.7
${AXIS_HOME}/bin/setenv.sh
${AXIS_HOME}/bin/java2wsdl.sh -cn axis2.service.StateMachineService -cp build/classes \
    -tn "http://axis2.example" -stn "http://axis2.example/xsd"