package axis2.service;

import java.util.HashMap;
import java.util.Map;

public class StateMachineServiceImpl implements StateMachineService {

    private String state = "Initial";

    private static Map<String, String> transitions = new HashMap<>();
    static {
        transitions.put("Configure", "Ready");
        transitions.put("Start", "Running");
        transitions.put("Stop", "Ready");
    }

    @Override
    public String getState() {
        return this.state;
    }

    @Override
    public void processInput(String input) {
        String targetState = transitions.get(input);
        if (targetState == null) {
            targetState = "Undefined";
        }
        this.state = targetState;
    }

}
