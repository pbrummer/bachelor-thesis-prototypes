package axis2.service;

public interface StateMachineService {

    String getState();

    void processInput(String input);

}
