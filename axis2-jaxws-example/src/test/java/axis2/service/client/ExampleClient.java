package axis2.service.client;

import javax.xml.namespace.QName;
import java.net.URL;

public class ExampleClient {

    public static void main(String[] args) throws Exception {
        URL endpoint = new URL("http://localhost:8080/axis2/services/StateMachineService");
        QName qname = new QName("http://service.axis2/","StateMachineService");

        StateMachineService_Service service = new StateMachineService_Service(endpoint, qname);
        StateMachineService serviceImpl = service.getStateMachineServiceImplPort();


        System.out.println(serviceImpl.getState());
        serviceImpl.processInput("Stop");
        System.out.println(serviceImpl.getState());
        serviceImpl.processInput("Start");
        System.out.println(serviceImpl.getState());
    }

}
