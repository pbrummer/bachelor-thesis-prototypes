import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {FunctionManagerClientService} from '../function-manager-client.service';
import 'rxjs/add/operator/takeUntil';

@Component({
  selector: 'app-fm-list',
  templateUrl: './fm-list.component.html',
  styleUrls: ['./fm-list.component.css']
})
export class FmListComponent implements OnInit, OnDestroy {

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  instances: string[];

  constructor(private functionManagerClient: FunctionManagerClientService) {
  }

  ngOnInit() {
    const that = this;

    this.functionManagerClient.continuousRequest(this.functionManagerClient.listRunning(), 2000)
      .takeUntil(this.ngUnsubscribe)
      .subscribe(
        runningFMs => {
          this.instances = runningFMs;
        }
      );
  }

  ngOnDestroy() {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.complete();
  }

}
