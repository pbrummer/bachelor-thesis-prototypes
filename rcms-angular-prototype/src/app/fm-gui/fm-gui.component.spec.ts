import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FmGuiComponent } from './fm-gui.component';

describe('FmGuiComponent', () => {
  let component: FmGuiComponent;
  let fixture: ComponentFixture<FmGuiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FmGuiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FmGuiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
