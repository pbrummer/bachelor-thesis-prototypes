import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FmGuiComponent} from '../fm-gui/fm-gui.component';
import {FmListComponent} from '../fm-list/fm-list.component';

const routes: Routes = [
  {path: '', component: FmListComponent},
  {path: 'fm/:instance', component: FmGuiComponent}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  declarations: []
})
export class AppRoutingModule { }
