export class FunctionManagerConfiguration {

  identifier: string;

  fmClass: string;

  instanceUrl: string;

}
