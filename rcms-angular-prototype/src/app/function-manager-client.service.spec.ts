import { TestBed, inject } from '@angular/core/testing';

import { FunctionManagerClientService } from './function-manager-client.service';

describe('FunctionManagerClientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FunctionManagerClientService]
    });
  });

  it('should be created', inject([FunctionManagerClientService], (service: FunctionManagerClientService) => {
    expect(service).toBeTruthy();
  }));
});
