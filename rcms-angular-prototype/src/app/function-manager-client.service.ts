import {Injectable} from '@angular/core';
import {environment} from '../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {FunctionManagerConfiguration} from './data/function-manager-configuration';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/retry';

@Injectable()
export class FunctionManagerClientService {

  private apiPath = environment.apiPath + 'functionmanager/';

  constructor(private http: HttpClient) {
  }

  create(fmConfiguration: FunctionManagerConfiguration): void {
    this.http.post(this.apiPath, fmConfiguration);
  }

  destroy(identifier: string): void {
    this.http.delete(this.apiPath + identifier);
  }

  listRunning(): Observable<string[]> {
    return this.http.get<string[]>(this.apiPath + 'running');
  }

  continuousRequest<T>(request: Observable<T>, interval: number): Observable<T> {
    return Observable.interval(interval).startWith(1).flatMap(() => request).retry(2);
  }

}
