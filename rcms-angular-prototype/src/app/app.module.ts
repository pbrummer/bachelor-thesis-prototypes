import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing/app-routing.module';

import {AppComponent} from './app.component';
import {FmGuiComponent} from './fm-gui/fm-gui.component';
import {RouterModule} from '@angular/router';
import {FmListComponent} from './fm-list/fm-list.component';
import {FunctionManagerClientService} from './function-manager-client.service';
import {HttpClientModule} from '@angular/common/http';


@NgModule({
  declarations: [
    AppComponent,
    FmGuiComponent,
    FmListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    HttpClientModule
  ],
  providers: [
    FunctionManagerClientService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
