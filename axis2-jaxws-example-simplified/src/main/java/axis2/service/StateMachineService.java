package axis2.service;

import javax.jws.WebService;

@WebService
public interface StateMachineService {

    String getState();

    void processInput(String input);

}
