package axis2.service;

import javax.jws.WebService;
import java.util.HashMap;
import java.util.Map;

@WebService(endpointInterface = "axis2.service.StateMachineService",
            serviceName = "StateMachineService",
            wsdlLocation = "META-INF/StateMachineService.wsdl")
public class StateMachineServiceImpl implements StateMachineService {

    private static String state = "Initial";

    private static Map<String, String> transitions = new HashMap<>();
    static {
        transitions.put("Configure", "Ready");
        transitions.put("Start", "Running");
        transitions.put("Stop", "Ready");
    }

    @Override
    public String getState() {
        return state;
    }

    @Override
    public void processInput(String input) {
        String targetState = transitions.get(input);
        if (targetState == null) {
            targetState = "Undefined";
        }
        state = targetState;
    }

}