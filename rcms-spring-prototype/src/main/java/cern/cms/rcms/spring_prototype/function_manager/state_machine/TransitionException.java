package cern.cms.rcms.spring_prototype.function_manager.state_machine;

public class TransitionException extends Exception {

    public TransitionException() {
    }

    public TransitionException(String message) {
        super(message);
    }

    public TransitionException(Throwable cause) {
        super(cause);
    }

    public TransitionException(String message, Throwable cause) {
        super(message, cause);
    }


}
