package cern.cms.rcms.spring_prototype.api.resources.notification;

import cern.cms.rcms.spring_prototype.function_manager.state_machine.StateChangeNotification;

public interface NotificationResource {

    void sendStateChangeNotification(String fm, StateChangeNotification notification);

}
