package cern.cms.rcms.spring_prototype.function_manager.state_machine;

public interface TransitionHandler {

    void handleTransition(State from, Input input, State to) throws TransitionException;

}
