package cern.cms.rcms.spring_prototype.api.service.parameter;

import cern.cms.rcms.spring_prototype.api.resources.parameter.ParameterResource;
import cern.cms.rcms.spring_prototype.function_manager.FunctionManager;
import cern.cms.rcms.spring_prototype.function_manager.parameter.Parameter;
import cern.cms.rcms.spring_prototype.function_manager.parameter.ParameterSet;
import cern.cms.rcms.spring_prototype.run_control.FunctionManagerRepository;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/parameter/{fm}")
public class ParameterController implements ParameterResource {

    private FunctionManagerRepository functionManagerRepository;

    public ParameterController(FunctionManagerRepository functionManagerRepository) {
        this.functionManagerRepository = functionManagerRepository;
    }

    @Override
    @RequestMapping(value = "/{parameter}", method = RequestMethod.GET)
    public Parameter getParameter(@PathVariable(value = "fm") String fm, @PathVariable(value = "parameter") String name) {
        FunctionManager functionManager = this.functionManagerRepository.getInstance(fm);
        if (functionManager == null) {
            return null;
        }
        return functionManager.getParameterSet().getParameter(name);
    }

    @Override
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public ParameterSet getParameters(@PathVariable(value = "fm") String fm) {
        FunctionManager functionManager = this.functionManagerRepository.getInstance(fm);
        if (functionManager == null) {
            return null;
        }
        return functionManager.getParameterSet();
    }

    @Override
    @RequestMapping(method = RequestMethod.PUT)
    public void setParameter(@PathVariable(value = "fm") String fm, @RequestBody Parameter parameter) {
        FunctionManager functionManager = this.functionManagerRepository.getInstance(fm);
        if (functionManager == null) {
            return;
        }
        functionManager.getParameterSet().setParameter(parameter);
    }
}
