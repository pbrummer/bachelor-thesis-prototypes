package cern.cms.rcms.spring_prototype.api.resources.function_manager;

import cern.cms.rcms.spring_prototype.configuration.FunctionManagerConfiguration;

import javax.validation.constraints.NotNull;
import java.util.Set;

public interface FunctionManagerResource {

    void create(@NotNull FunctionManagerConfiguration fmConfiguration);

    void destroy(@NotNull String identifier);

    Set<String> listRunning();

}
