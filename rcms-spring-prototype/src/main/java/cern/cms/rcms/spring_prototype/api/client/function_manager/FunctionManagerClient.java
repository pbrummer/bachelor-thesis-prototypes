package cern.cms.rcms.spring_prototype.api.client.function_manager;

import cern.cms.rcms.spring_prototype.api.resources.function_manager.FunctionManagerResource;
import cern.cms.rcms.spring_prototype.configuration.FunctionManagerConfiguration;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FunctionManagerClient implements FunctionManagerResource {

    private static final Map<String, String> emptyParameterMap
            = new HashMap<>(0);

    private RestTemplate restTemplate;
    private String baseUrl;

    public FunctionManagerClient(RestTemplate restTemplate, String baseUrl) {
        this.restTemplate = restTemplate;

        this.baseUrl = baseUrl + "functionmanager/";
    }

    @Override
    public void create(FunctionManagerConfiguration fmConfiguration) {
        this.restTemplate.postForLocation(
                this.baseUrl,
                fmConfiguration);
    }

    @Override
    public void destroy(String identifier) {
        this.restTemplate.delete(this.baseUrl + identifier);
    }

    @Override
    public Set<String> listRunning() {
        return this.restTemplate.getForObject(
                this.baseUrl + "running",
                Set.class);
    }

}
