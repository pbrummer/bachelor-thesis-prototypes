package cern.cms.rcms.spring_prototype.function_manager.state_machine.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Transition {

    public static final String ANY = "ANY";

    String from() default ANY;

    String input() default ANY;

    String to() default ANY;

}
