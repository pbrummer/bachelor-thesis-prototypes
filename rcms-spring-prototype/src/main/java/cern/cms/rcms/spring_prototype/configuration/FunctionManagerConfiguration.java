package cern.cms.rcms.spring_prototype.configuration;

import java.util.HashSet;
import java.util.Set;

public class FunctionManagerConfiguration {

    private String identifier;
    private String fmClass;
    private String instanceUrl;

    public FunctionManagerConfiguration(String identifier, String fmClass, String instanceUrl) {
        this.identifier = identifier;
        this.fmClass = fmClass;
        this.instanceUrl = instanceUrl;
    }

    private Set<FunctionManagerConfiguration> childFunctionManagers
            = new HashSet<>();

    public Set<FunctionManagerConfiguration> getChildFunctionManagers() {
        return childFunctionManagers;
    }

    private FunctionManagerConfiguration() {
    }

    public String getIdentifier() {
        return identifier;
    }

    public String getFmClass() {
        return fmClass;
    }

    public String getInstanceUrl() {
        return instanceUrl;
    }

}
