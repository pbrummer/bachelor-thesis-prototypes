package cern.cms.rcms.spring_prototype.function_manager.parameter;

public interface ParameterChangeListener {

    void parameterChanged(Parameter parameter);

}
