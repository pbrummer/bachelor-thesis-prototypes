package cern.cms.rcms.spring_prototype.function_manager;

import cern.cms.rcms.spring_prototype.api.client.ClientService;
import cern.cms.rcms.spring_prototype.configuration.FunctionManagerConfiguration;
import cern.cms.rcms.spring_prototype.function_manager.parameter.ParameterSet;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.StateChangeNotification;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.StateMachine;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.StateMachines;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.TransitionHandler;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.annotations.AnnotatedTransitionHandler;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.annotations.Transition;

import java.lang.reflect.Method;

public abstract class FunctionManager {

    private StateMachine stateMachine = StateMachines.defaultStateMachine();

    private ParameterSet parameters = new ParameterSet();

    private ClientService clientService;

    private FunctionManagerConfiguration configuration;

    public void setClientService(ClientService clientService) {
        this.clientService = clientService;
    }

    public ClientService getClientService() {
        return this.clientService;
    }

    public StateMachine getStateMachine() {
        return this.stateMachine;
    }

    public ParameterSet getParameterSet() {
        return this.parameters;
    }

    public FunctionManagerConfiguration getConfiguration() {
        return this.configuration;
    }

    public void setConfiguration(FunctionManagerConfiguration fmConfiguration) {
        this.configuration = fmConfiguration;
    }

    protected void registerTransitionHandlers(Object instance) {
        for (Method method : instance.getClass().getDeclaredMethods()) {
            if (method.isAnnotationPresent(Transition.class)) {
                Transition transition = method.getAnnotation(Transition.class);
                TransitionHandler transitionHandler = new AnnotatedTransitionHandler(instance, method, transition);
                this.stateMachine.addTransitionHandler(transitionHandler);
            }
        }
    }

    public void create() {
        this.userCreate();
    }

    public void destroy() {
        this.userDestroy();
    }

    public abstract void handleStateChangeNotification(StateChangeNotification notification);

    protected abstract void userCreate();

    protected abstract void userDestroy();

}
