package cern.cms.rcms.spring_prototype;

import cern.cms.rcms.spring_prototype.api.client.ClientService;
import cern.cms.rcms.spring_prototype.api.client.function_manager.FunctionManagerClient;
import cern.cms.rcms.spring_prototype.api.client.parameter.ParameterClient;
import cern.cms.rcms.spring_prototype.api.client.state_machine.StateMachineClient;
import cern.cms.rcms.spring_prototype.configuration.FunctionManagerConfiguration;
import cern.cms.rcms.spring_prototype.fm.l1.L1FunctionManager;
import cern.cms.rcms.spring_prototype.fm.top.TopFunctionManager;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.Input;
import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class Application {

    private static final Logger logger = Logger.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CommandLineRunner run(ClientService clientService) throws Exception {
        return args -> {
            String localRunControl = "http://localhost:8080/";

            FunctionManagerConfiguration topFmConfiguration = new FunctionManagerConfiguration(
                    "top",
                    TopFunctionManager.class.getName(),
                    localRunControl
            );

            FunctionManagerConfiguration l1aFmConfiguration = new FunctionManagerConfiguration(
                    "l1a",
                    L1FunctionManager.class.getName(),
                    localRunControl
            );

            FunctionManagerConfiguration l1bFmConfiguration = new FunctionManagerConfiguration(
                    "l1b",
                    L1FunctionManager.class.getName(),
                    localRunControl
            );

            topFmConfiguration.getChildFunctionManagers().add(l1aFmConfiguration);
            topFmConfiguration.getChildFunctionManagers().add(l1bFmConfiguration);

            FunctionManagerClient fmClient = clientService.createFunctionManagerClient(localRunControl);
            ParameterClient parameterClient = clientService.createParameterClient(localRunControl);
            StateMachineClient stateMachineClient = clientService.createStateMachineClient(localRunControl);

            fmClient.create(topFmConfiguration);

            String topFmId = topFmConfiguration.getIdentifier();
            String l1aFmId = l1aFmConfiguration.getIdentifier();
            String l1bFmId = l1bFmConfiguration.getIdentifier();

            logger.info(stateMachineClient.getState(topFmId).getName());
            logger.info(stateMachineClient.getState(l1aFmId).getName());
            logger.info(stateMachineClient.getState(l1bFmId).getName());

            stateMachineClient.sendInput(topFmId, new Input("Configure"));

            logger.info(stateMachineClient.getState(topFmId).getName());
            logger.info(stateMachineClient.getState(l1aFmId).getName());
            logger.info(stateMachineClient.getState(l1bFmId).getName());

            logger.info(parameterClient.getParameter(l1aFmId, "Configuration").getValue());
            logger.info(parameterClient.getParameter(l1bFmId, "Configuration").getValue());

            stateMachineClient.sendInput(topFmId, new Input("Start"));

            logger.info(stateMachineClient.getState(topFmId).getName());
            logger.info(stateMachineClient.getState(l1aFmId).getName());
            logger.info(stateMachineClient.getState(l1bFmId).getName());

            logger.info(parameterClient.getParameter(l1aFmId, "runNumber").getValue());
            logger.info(parameterClient.getParameter(l1bFmId, "runNumber").getValue());

            stateMachineClient.sendInput(topFmId, new Input("Stop"));

            logger.info(stateMachineClient.getState(topFmId).getName());
            logger.info(stateMachineClient.getState(l1aFmId).getName());
            logger.info(stateMachineClient.getState(l1bFmId).getName());

            stateMachineClient.sendInput(topFmId, new Input("Halt"));

            logger.info(stateMachineClient.getState(topFmId).getName());
            logger.info(stateMachineClient.getState(l1aFmId).getName());
            logger.info(stateMachineClient.getState(l1bFmId).getName());

        };
    }

}
