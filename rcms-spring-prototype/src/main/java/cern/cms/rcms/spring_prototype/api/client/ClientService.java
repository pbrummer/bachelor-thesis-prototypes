package cern.cms.rcms.spring_prototype.api.client;

import cern.cms.rcms.spring_prototype.api.client.function_manager.FunctionManagerClient;
import cern.cms.rcms.spring_prototype.api.client.notification.NotificationClient;
import cern.cms.rcms.spring_prototype.api.client.parameter.ParameterClient;
import cern.cms.rcms.spring_prototype.api.client.state_machine.StateMachineClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ClientService {

    private RestTemplate restTemplate;

    public ClientService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public FunctionManagerClient createFunctionManagerClient(String baseUrl) {
        return new FunctionManagerClient(this.restTemplate, baseUrl);
    }

    public ParameterClient createParameterClient(String baseUrl) {
        return new ParameterClient(this.restTemplate, baseUrl);
    }

    public StateMachineClient createStateMachineClient(String baseUrl) {
        return new StateMachineClient(this.restTemplate, baseUrl);
    }

    public NotificationClient createNotificationClient(String baseUrl) {
        return new NotificationClient(this.restTemplate, baseUrl);
    }

}
