package cern.cms.rcms.spring_prototype.api.client.notification;

import cern.cms.rcms.spring_prototype.api.resources.notification.NotificationResource;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.StateChangeNotification;
import org.springframework.web.client.RestTemplate;

public class NotificationClient implements NotificationResource {

    private RestTemplate restTemplate;
    private String baseUrl;

    public NotificationClient(RestTemplate restTemplate, String baseUrl) {
        this.restTemplate = restTemplate;

        this.baseUrl = baseUrl + "notification/";
    }

    @Override
    public void sendStateChangeNotification(String fm, StateChangeNotification notification) {
        this.restTemplate.put(this.baseUrl + fm + "/state", notification);
    }

}
