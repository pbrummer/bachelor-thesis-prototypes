package cern.cms.rcms.spring_prototype.function_manager.state_machine.annotations;

import cern.cms.rcms.spring_prototype.function_manager.state_machine.Input;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.State;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.TransitionException;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.TransitionHandler;
import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AnnotatedTransitionHandler implements TransitionHandler {

    private static final Logger logger = Logger.getLogger(AnnotatedTransitionHandler.class);

    private Object handlerInstance;
    private Method handlerMethod;
    private Transition annotation;

    public AnnotatedTransitionHandler(Object handlerInstance, Method handlerMethod, Transition annotation) {
        this.handlerInstance = handlerInstance;
        this.handlerMethod = handlerMethod;
        this.annotation = annotation;
    }

    @Override
    public void handleTransition(State from, Input input, State to) throws TransitionException {
        if (!this.annotation.from().equals(Transition.ANY)
                && !this.annotation.from().equals(from.getName())) {
            return;
        }
        if (!this.annotation.input().equals(Transition.ANY)
                && !this.annotation.input().equals(input.getName())) {
            return;
        }
        if (!this.annotation.to().equals(Transition.ANY)
                && !this.annotation.to().equals(to.getName())) {
            return;
        }
        try {
            this.handlerMethod.invoke(this.handlerInstance, from, input, to);
        } catch (IllegalAccessException e) {
            throw new TransitionException("Calling transition method failed due to an access exception.", e);
        } catch (InvocationTargetException e) {
            throw new TransitionException("Calling transition method failed due to an invocation target exception.", e);
        }
    }
}
