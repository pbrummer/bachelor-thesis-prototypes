package cern.cms.rcms.spring_prototype.api.service.function_manager;

import cern.cms.rcms.spring_prototype.api.client.ClientService;
import cern.cms.rcms.spring_prototype.api.resources.function_manager.FunctionManagerResource;
import cern.cms.rcms.spring_prototype.configuration.FunctionManagerConfiguration;
import cern.cms.rcms.spring_prototype.function_manager.FunctionManager;
import cern.cms.rcms.spring_prototype.run_control.FunctionManagerRepository;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping(value = "/functionmanager")
public class FunctionManagerController implements FunctionManagerResource {

    private static final Logger logger = Logger.getLogger(FunctionManagerController.class);

    private FunctionManagerRepository functionManagerRepository;

    private ClientService clientService;

    public FunctionManagerController(FunctionManagerRepository functionManagerRepository, ClientService clientService) {
        this.functionManagerRepository = functionManagerRepository;
        this.clientService = clientService;
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public void create(@RequestBody FunctionManagerConfiguration fmConfiguration) {
        try {
            Class fmClass = Class.forName(fmConfiguration.getFmClass());
            if (!FunctionManager.class.isAssignableFrom(fmClass)) {
                logger.warn("Creation request for a class which is not extending the FunctionManager class.");
                return;
            }
            FunctionManager instance = (FunctionManager) fmClass.newInstance();
            instance.setClientService(this.clientService);
            instance.setConfiguration(fmConfiguration);
            instance.create();
            this.functionManagerRepository.addInstance(fmConfiguration.getIdentifier(), instance);
        } catch (ClassNotFoundException e) {
            logger.warn("Creation request for unknown FM class.", e);
        } catch (IllegalAccessException | InstantiationException e) {
            logger.warn("Error instantiating Function Manager class.", e);
        }
    }

    @Override
    @RequestMapping(value = "/{fm}", method = RequestMethod.DELETE)
    public void destroy(@PathVariable(value = "fm") String identifier) {
        FunctionManager instance = this.functionManagerRepository.getInstance(identifier);
        if (instance == null) {
            return;
        }
        instance.destroy();
        this.functionManagerRepository.removeInstance(identifier);
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/running")
    public Set<String> listRunning() {
        return this.functionManagerRepository.getInstanceNames();
    }

}
