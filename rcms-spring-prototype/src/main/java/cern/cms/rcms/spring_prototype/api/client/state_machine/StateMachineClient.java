package cern.cms.rcms.spring_prototype.api.client.state_machine;

import cern.cms.rcms.spring_prototype.api.resources.state_machine.StateMachineResource;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.Input;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.State;
import org.springframework.web.client.RestTemplate;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class StateMachineClient implements StateMachineResource {

    private RestTemplate restTemplate;
    private String baseUrl;

    public StateMachineClient(RestTemplate restTemplate, String baseUrl) {
        this.restTemplate = restTemplate;

        this.baseUrl = baseUrl + "statemachine/";
    }

    @Override
    public void sendInput(String fm, Input input) {
        this.restTemplate.put(this.baseUrl + fm, input);
    }

    @Override
    public State getState(String fm) {
        return this.restTemplate.getForObject(
                this.baseUrl + fm,
                State.class);
    }

    @Override
    public Map<String, Map<String, String>> getTransitions(String fm) {
        return this.restTemplate.getForObject(
                this.baseUrl + fm + "/transitions",
                Map.class);
    }

    @Override
    public Set<String> getStates(String fm) {
        return this.restTemplate.getForObject(
                this.baseUrl + fm + "/states",
                Set.class);
    }

}
