package cern.cms.rcms.spring_prototype.function_manager.state_machine;

import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

public class StateMachines {

    private static final Logger logger = Logger.getLogger(StateMachines.class);

    public static final State ErrorState = new State("Error");
    public static final State ReadyState = new State("Ready");
    public static final State ConfiguredState = new State("Configured");
    public static final State RunningState = new State("Running");

    public static final Input ConfigureInput = new Input("Configure");
    public static final Input HaltInput = new Input("Halt");
    public static final Input StartInput = new Input("Start");
    public static final Input StopInput = new Input("Stop");
    public static final Input ResetInput = new Input("Reset");

    private static final Set<State> defaultStateMachineStates = new HashSet<>();
    static {
        defaultStateMachineStates.add(ErrorState);
        defaultStateMachineStates.add(ReadyState);
        defaultStateMachineStates.add(ConfiguredState);
        defaultStateMachineStates.add(RunningState);
    }

    private static final State defaultStateMachineInitialState = ReadyState;
    private static final State defaultStateMachineErrorState = ErrorState;

    public static StateMachine defaultStateMachine() {
        StateMachine defaultStateMachine = null;
        try {
            defaultStateMachine = new StateMachine(defaultStateMachineStates, defaultStateMachineInitialState, defaultStateMachineErrorState);

            defaultStateMachine.addTransition(ReadyState, ConfigureInput, ConfiguredState);
            defaultStateMachine.addTransition(ConfiguredState, HaltInput, ReadyState);
            defaultStateMachine.addTransition(ConfiguredState, StartInput, RunningState);
            defaultStateMachine.addTransition(RunningState, StopInput, ConfiguredState);
            defaultStateMachine.addTransition(ErrorState, ResetInput, ReadyState);
        } catch (StateMachineException e) {
            logger.error("Error creating default state machine.", e);
        }
        return defaultStateMachine;
    }

}
