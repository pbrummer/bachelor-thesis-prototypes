package cern.cms.rcms.spring_prototype.function_manager.state_machine;

public class State {

    private String name;

    public State(String name) {
        this.name = name;
    }

    private State() {}

    public String getName() {
        return this.name;
    }

}
