package cern.cms.rcms.spring_prototype.api.service.notification;

import cern.cms.rcms.spring_prototype.api.resources.notification.NotificationResource;
import cern.cms.rcms.spring_prototype.function_manager.FunctionManager;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.StateChangeNotification;
import cern.cms.rcms.spring_prototype.run_control.FunctionManagerRepository;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping(value = "/notification/{fm}")
public class NotificationController implements NotificationResource {

    private FunctionManagerRepository functionManagerRepository;

    public NotificationController(FunctionManagerRepository functionManagerRepository) {
        this.functionManagerRepository = functionManagerRepository;
    }

    @Override
    @RequestMapping(value = "/state", method = RequestMethod.PUT)
    public void sendStateChangeNotification(@PathVariable(value = "fm") String fm, @RequestBody StateChangeNotification notification) {
        FunctionManager functionManager = this.functionManagerRepository.getInstance(fm);
        if (functionManager == null) {
            return;
        }
        functionManager.handleStateChangeNotification(notification);
    }

}
