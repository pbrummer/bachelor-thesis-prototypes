package cern.cms.rcms.spring_prototype.api.service.state_machine;

import cern.cms.rcms.spring_prototype.api.resources.state_machine.StateMachineResource;
import cern.cms.rcms.spring_prototype.function_manager.FunctionManager;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.Input;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.State;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.StateMachineException;
import cern.cms.rcms.spring_prototype.run_control.FunctionManagerRepository;
import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping(value = "/statemachine/{fm}")
public class StateMachineController implements StateMachineResource {

    private static final Logger logger = Logger.getLogger(StateMachineController.class);

    private FunctionManagerRepository functionManagerRepository;

    public StateMachineController(FunctionManagerRepository functionManagerRepository) {
        this.functionManagerRepository = functionManagerRepository;
    }

    @Override
    @RequestMapping(method = RequestMethod.PUT)
    public void sendInput(@PathVariable(value = "fm") String fm, @RequestBody Input input) {
        FunctionManager functionManager = this.functionManagerRepository.getInstance(fm);
        if (functionManager == null) {
            return;
        }
        try {
            functionManager.getStateMachine().transition(input);
        } catch (StateMachineException e) {
            logger.error(String.format("Error sending input to the state machine of Function Manager '%s'.", fm), e);
        }
    }

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public State getState(@PathVariable(value = "fm") String fm) {
        FunctionManager functionManager = this.functionManagerRepository.getInstance(fm);
        if (functionManager == null) {
            return null;
        }
        return functionManager.getStateMachine().getCurrentState();
    }

    @Override
    @RequestMapping(value = "/transitions", method = RequestMethod.GET)
    public Map<String, Map<String, String>> getTransitions(String fm) {
        FunctionManager functionManager = this.functionManagerRepository.getInstance(fm);
        if (functionManager == null) {
            return null;
        }
        return functionManager.getStateMachine().getTransitions();
    }

    @Override
    @RequestMapping(value = "/states", method = RequestMethod.GET)
    public Set<String> getStates(String fm) {
        FunctionManager functionManager = this.functionManagerRepository.getInstance(fm);
        if (functionManager == null) {
            return null;
        }
        return functionManager.getStateMachine().getStates();
    }
}

