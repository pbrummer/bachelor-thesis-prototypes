package cern.cms.rcms.spring_prototype.api.resources.parameter;

import cern.cms.rcms.spring_prototype.function_manager.parameter.Parameter;
import cern.cms.rcms.spring_prototype.function_manager.parameter.ParameterSet;

import javax.validation.constraints.NotNull;

public interface ParameterResource {

    Parameter getParameter(@NotNull String fm, @NotNull String name);

    ParameterSet getParameters(@NotNull String fm);

    void setParameter(@NotNull String fm, @NotNull Parameter parameter);

}
