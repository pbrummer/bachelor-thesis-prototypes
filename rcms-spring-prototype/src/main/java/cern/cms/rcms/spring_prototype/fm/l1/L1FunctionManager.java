package cern.cms.rcms.spring_prototype.fm.l1;

import cern.cms.rcms.spring_prototype.api.client.notification.NotificationClient;
import cern.cms.rcms.spring_prototype.function_manager.FunctionManager;
import cern.cms.rcms.spring_prototype.function_manager.parameter.Parameter;
import cern.cms.rcms.spring_prototype.function_manager.parameter.ParameterSet;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.Input;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.State;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.StateChangeNotification;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.TransitionException;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.annotations.Transition;
import org.apache.log4j.Logger;

public class L1FunctionManager extends FunctionManager {

    private static final Logger logger = Logger.getLogger(L1FunctionManager.class);

    private static final String parentFMurl = "http://localhost:8080/";

    private Parameter<String> stateParameter = new Parameter<>(
            "State",
            super.getStateMachine().getCurrentState().getName());

    private Parameter<String> configurationParameter = new Parameter<>(
            "Configuration",
            "");

    private Parameter<Integer> runNumberParameter = new Parameter<>(
            "runNumber",
            0);

    private NotificationClient parentFmNotificationClient;

    public L1FunctionManager() {
        super.registerTransitionHandlers(this);

        super.getParameterSet().setParameter(this.stateParameter);
        super.getParameterSet().setParameter(this.configurationParameter);
        super.getParameterSet().setParameter(this.runNumberParameter);
    }

    @Transition
    public void allTransitions(State from, Input input, State to) throws TransitionException {
        logger.info(String.format("Received transition from %s to %s with input %s.",
                from.getName(), to.getName(), input.getName()));

        this.stateParameter.setValue(to.getName());

        this.parentFmNotificationClient.sendStateChangeNotification(
                "top",
                new StateChangeNotification(super.getConfiguration().getIdentifier(), to));
    }

    @Transition(to = "Error")
    public void errorTransition(State from, Input input, State to) {
    }

    @Transition(input = "Configure")
    public void configureAction(State from, Input input, State to) throws TransitionException {
        sleepIgnore(5000);

        ParameterSet parameterSet = input.getParameters();

        Parameter parameter = parameterSet.getParameter("Configuration");
        String configuration = (String) parameter.getValue();

        this.configurationParameter.setValue(configuration);
    }

    @Transition(input = "Start")
    public void startAction(State from, Input input, State to) throws TransitionException {
        sleepIgnore(2000);

        ParameterSet parameterSet = input.getParameters();

        Parameter parameter = parameterSet.getParameter("runNumber");
        Integer runNumber = (Integer) parameter.getValue();

        this.runNumberParameter.setValue(runNumber);
    }

    @Transition(input = "Stop")
    public void stopAction(State from, Input input, State to) throws TransitionException {
        sleepIgnore(2000);

        this.runNumberParameter.setValue(0);
    }

    @Transition(input = "Halt")
    public void haltAction(State from, Input input, State to) throws TransitionException {
        sleepIgnore(5000);

        throw new TransitionException("Something went wrong.");
    }

    @Transition(input = "Reset")
    public void resetAction(State from, Input input, State to) throws TransitionException {
        sleepIgnore(2000);

        this.runNumberParameter.setValue(0);
        this.configurationParameter.setValue("");
    }

    private static void sleepIgnore(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            logger.error(e);
        }
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification notification) {
    }

    @Override
    public void userCreate() {
        this.parentFmNotificationClient = super.getClientService().createNotificationClient(parentFMurl);
    }

    @Override
    public void userDestroy() {

    }
}
