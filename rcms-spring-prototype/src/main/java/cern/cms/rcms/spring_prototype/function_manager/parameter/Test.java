package cern.cms.rcms.spring_prototype.function_manager.parameter;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Test {

    public static void main(String[] args) throws IOException {

        Map<String, Parameter> map = new HashMap<>();
        map.put("one", new Parameter<>("one", "value"));
        map.put("two", new Parameter<>("two", "value"));

        Parameter<Map> param = new Parameter<>("test", map);

        ObjectMapper m = new ObjectMapper();

        String json = m.writeValueAsString(param);

        System.out.println(json);

        Parameter<?> param2 = m.readValue(json, Parameter.class);

        System.out.println("end");
    }

}
