package cern.cms.rcms.spring_prototype.api.client.parameter;

import cern.cms.rcms.spring_prototype.api.resources.parameter.ParameterResource;
import cern.cms.rcms.spring_prototype.function_manager.parameter.Parameter;
import cern.cms.rcms.spring_prototype.function_manager.parameter.ParameterSet;
import org.springframework.http.HttpEntity;
import org.springframework.web.client.RestTemplate;

public class ParameterClient implements ParameterResource {

    private RestTemplate restTemplate;
    private String baseUrl;

    public ParameterClient(RestTemplate restTemplate, String baseUrl) {
        this.restTemplate = restTemplate;

        this.baseUrl = baseUrl + "parameter/";
    }

    @Override
    public Parameter getParameter(String fm, String name) {
        return this.restTemplate.getForObject(
                this.baseUrl + fm + "/" + name,
                Parameter.class);
    }

    @Override
    public ParameterSet getParameters(String fm) {
        return this.restTemplate.getForObject(
                this.baseUrl + fm + "/all",
                ParameterSet.class);
    }

    @Override
    public void setParameter(String fm, Parameter parameter) {
        HttpEntity<Parameter> requestEntity
                = new HttpEntity<>(parameter);

        this.restTemplate.put(
                this.baseUrl + fm + "/" + parameter.getName(),
                requestEntity);
    }

}
