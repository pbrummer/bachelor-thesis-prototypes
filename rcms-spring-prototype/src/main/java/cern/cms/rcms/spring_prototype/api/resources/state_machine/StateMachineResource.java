package cern.cms.rcms.spring_prototype.api.resources.state_machine;

import cern.cms.rcms.spring_prototype.function_manager.state_machine.Input;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.State;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

public interface StateMachineResource {

    void sendInput(@NotNull String fm, @NotNull Input input);

    State getState(@NotNull String fm);

    Map<String, Map<String, String>> getTransitions(@NotNull String fm);

    Set<String> getStates(@NotNull String fm);
}
