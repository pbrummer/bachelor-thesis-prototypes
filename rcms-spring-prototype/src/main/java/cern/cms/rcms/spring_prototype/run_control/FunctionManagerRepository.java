package cern.cms.rcms.spring_prototype.run_control;

import cern.cms.rcms.spring_prototype.function_manager.FunctionManager;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Repository
public class FunctionManagerRepository {

    private Map<String, FunctionManager> instances = new ConcurrentHashMap<>();

    public FunctionManager getInstance(String identifier) {
        return this.instances.get(identifier);
    }

    public void addInstance(String identifier, FunctionManager functionManager) {
        this.instances.put(identifier, functionManager);
    }

    public void removeInstance(String identifier) {
        this.instances.remove(identifier);
    }

    public Set<String> getInstanceNames() {
        return this.instances.keySet();
    }
}
