package cern.cms.rcms.spring_prototype.fm.top;

import cern.cms.rcms.spring_prototype.api.client.function_manager.FunctionManagerClient;
import cern.cms.rcms.spring_prototype.api.client.state_machine.StateMachineClient;
import cern.cms.rcms.spring_prototype.function_manager.FunctionManager;
import cern.cms.rcms.spring_prototype.function_manager.parameter.Parameter;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.Input;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.State;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.StateChangeNotification;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.TransitionException;
import cern.cms.rcms.spring_prototype.function_manager.state_machine.annotations.Transition;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class TopFunctionManager extends FunctionManager {

    private static final Logger logger = Logger.getLogger(TopFunctionManager.class);

    private AtomicLong runNumber = new AtomicLong(0);

    private Map<String, FunctionManagerClient> childFmFunctionManagerClients = new ConcurrentHashMap<>();
    private Map<String, StateMachineClient> childFmStateMachineClients = new ConcurrentHashMap<>();

    private Map<String, State> childStates = new ConcurrentHashMap<>();

    public TopFunctionManager() {
        super.registerTransitionHandlers(this);
    }

    @Transition
    public void allTransitions(State from, Input input, State to) throws TransitionException {
        logger.info(String.format("Received transition from %s to %s with input %s.",
                from.getName(), to.getName(), input.getName()));
    }

    @Transition(to = "Error")
    public void errorTransition(State from, Input input, State to) {
    }

    @Transition(input = "Configure")
    public void configureAction(State from, Input input, State to) throws TransitionException {
        Input configureInput = new Input("Configure");
        Parameter<String> configuration = new Parameter<>("Configuration", "cfg_value");
        configureInput.getParameters().setParameter(configuration);

        super.getConfiguration().getChildFunctionManagers().forEach((childFm) -> {
            this.childFmStateMachineClients.get(childFm.getIdentifier()).sendInput(childFm.getIdentifier(), configureInput);
        });
    }

    @Transition(input = "Start")
    public void startAction(State from, Input input, State to) throws TransitionException {
        Long runNumber = this.runNumber.incrementAndGet();

        Input startInput = new Input("Start");
        Parameter<Long> runNumberParameter = new Parameter<>("runNumber", runNumber);
        startInput.getParameters().setParameter(runNumberParameter);

        super.getConfiguration().getChildFunctionManagers().forEach((childFm) -> {
            this.childFmStateMachineClients.get(childFm.getIdentifier()).sendInput(childFm.getIdentifier(), startInput);
        });
    }

    @Transition(input = "Stop")
    public void stopAction(State from, Input input, State to) throws TransitionException {
        super.getConfiguration().getChildFunctionManagers().forEach((childFm) -> {
            this.childFmStateMachineClients.get(childFm.getIdentifier()).sendInput(childFm.getIdentifier(), input);
        });
    }

    @Transition(input = "Halt")
    public void haltAction(State from, Input input, State to) throws TransitionException {
        super.getConfiguration().getChildFunctionManagers().forEach((childFm) -> {
            this.childFmStateMachineClients.get(childFm.getIdentifier()).sendInput(childFm.getIdentifier(), input);
        });
    }

    @Transition(input = "Reset")
    public void resetAction(State from, Input input, State to) throws TransitionException {
        super.getConfiguration().getChildFunctionManagers().forEach((childFm) -> {
            this.childFmStateMachineClients.get(childFm.getIdentifier()).sendInput(childFm.getIdentifier(), input);
        });
    }

    @Override
    public void handleStateChangeNotification(StateChangeNotification notification) {
        this.childStates.put(notification.getIdentifier(), notification.getState());

        if (notification.getState().getName().equals("Error")) {
            this.getStateMachine().error(new Input(""));
        }

        logger.info(String.format("Child FM %s changed state to %s.",
                notification.getIdentifier(), notification.getState().getName()));
    }

    @Override
    public void userCreate() {
        super.getConfiguration().getChildFunctionManagers().forEach((childFm) -> {
            StateMachineClient smClient = super.getClientService().createStateMachineClient(childFm.getInstanceUrl());
            this.childFmStateMachineClients.put(childFm.getIdentifier(), smClient);

            FunctionManagerClient fmClient = super.getClientService().createFunctionManagerClient(childFm.getInstanceUrl());
            fmClient.create(childFm);
        });
    }

    @Override
    public void userDestroy() {

    }
}
