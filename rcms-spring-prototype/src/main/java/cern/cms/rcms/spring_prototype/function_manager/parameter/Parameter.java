package cern.cms.rcms.spring_prototype.function_manager.parameter;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "javaType")
public class Parameter<T> {

    private String name;
    private T value;

    public Parameter(String name, T value) {
        this.name = name;
        this.value = value;
    }

    private Parameter() {
    }

    public String getName() {
        return this.name;
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(T value) {
        this.value = value;
    }

}
