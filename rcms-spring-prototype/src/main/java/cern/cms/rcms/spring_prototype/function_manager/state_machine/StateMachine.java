package cern.cms.rcms.spring_prototype.function_manager.state_machine;

import org.apache.log4j.Logger;

import java.util.*;

public class StateMachine {

    private static final Logger logger = Logger.getLogger(StateMachine.class);

    private Map<String, State> statesByName;
    private Map<String, Map<String, String>> transitions = new HashMap<>();

    private State currentState;
    private State errorState;

    private Set<TransitionHandler> transitionHandlers = new HashSet<>();

    public StateMachine(Set<State> states, State initialState, State errorState) throws StateMachineException {
        if (!states.contains(initialState)) {
            throw new StateMachineException("Specified initial state is not a valid state.");
        }
        if (!states.contains(errorState)) {
            throw new StateMachineException("Specified error state is not a valid state.");
        }

        this.statesByName = new HashMap<>(2 * states.size());
        for (State state : states) {
            this.statesByName.put(state.getName(), state);
        }

        this.currentState = this.statesByName.get(initialState.getName());
        this.errorState = this.statesByName.get(errorState.getName());
    }

    public void addTransition(State from, Input input, State to) throws StateMachineException {
        if (!this.statesByName.containsKey(from.getName())) {
            throw new StateMachineException("Specified source state is not a valid state.");
        }
        if (!this.statesByName.containsKey(to.getName())) {
            throw new StateMachineException("Specified target state is not a valid state.");
        }

        Map<String, String> stateTransitions = this.transitions.get(from.getName());
        if (stateTransitions == null) {
            stateTransitions = new HashMap<>();
            this.transitions.put(from.getName(), stateTransitions);
        }
        stateTransitions.put(input.getName(), to.getName());
    }

    public void addTransitionHandler(TransitionHandler handler) {
        this.transitionHandlers.add(handler);
    }

    public State getCurrentState() {
        return this.currentState;
    }

    public synchronized void transition(Input input) throws StateMachineException {
        Map<String, String> transitions = this.transitions.get(this.currentState.getName());
        if (transitions == null) {
            throw new StateMachineException("Current state has no transitions.");
        }

        String inputName = input.getName();
        State targetState = this.statesByName.get(transitions.get(inputName));
        if (targetState == null) {
            throw new StateMachineException(String.format("Current state has no transition for input '%s'.", inputName));
        }

        boolean transitionFailure = false;

        for (TransitionHandler handler : this.transitionHandlers) {
            try {
                handler.handleTransition(this.currentState, input, targetState);
            } catch (TransitionException e) {
                transitionFailure = true;
            }
        }

        if (transitionFailure) {
            this.error(input);
        } else {
            this.currentState = targetState;
        }
    }

    public synchronized void error(Input input) {
        for (TransitionHandler handler : this.transitionHandlers) {
            try {
                handler.handleTransition(this.currentState, input, this.errorState);
            } catch (TransitionException e) {
                logger.error("Exception while processing error transition.", e);
            }
        }
        this.currentState = this.errorState;
    }

    public void setState(String state) throws StateMachineException {
        State targetState = this.statesByName.get(state);
        if (targetState == null) {
            throw new StateMachineException(String.format("Invalid target state %s.", state));
        }
        this.currentState = targetState;
    }

    public Map<String, Map<String, String>> getTransitions() {
        return this.transitions;
    }

    public Set<String> getStates() {
        return this.statesByName.keySet();
    }
}
