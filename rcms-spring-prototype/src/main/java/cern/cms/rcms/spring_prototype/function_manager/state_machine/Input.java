package cern.cms.rcms.spring_prototype.function_manager.state_machine;

import cern.cms.rcms.spring_prototype.function_manager.parameter.ParameterSet;

public class Input {

    private String name;

    private ParameterSet parameters;

    public Input(String name) {
        this.name = name;
        this.parameters = new ParameterSet();
    }

    public Input(Input input) {
        this.name = input.getName();
        this.parameters = new ParameterSet();
    }

    private Input() {}

    public String getName() {
        return this.name;
    }

    public ParameterSet getParameters() {
        return this.parameters;
    }

}
