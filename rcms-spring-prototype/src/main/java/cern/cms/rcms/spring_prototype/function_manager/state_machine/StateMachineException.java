package cern.cms.rcms.spring_prototype.function_manager.state_machine;

public class StateMachineException extends Exception {

    public StateMachineException() {
    }

    public StateMachineException(String message) {
        super(message);
    }

    public StateMachineException(Throwable cause) {
        super(cause);
    }

    public StateMachineException(String message, Throwable cause) {
        super(message, cause);
    }


}
