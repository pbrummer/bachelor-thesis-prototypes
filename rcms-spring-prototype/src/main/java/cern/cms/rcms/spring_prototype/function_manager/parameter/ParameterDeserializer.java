package cern.cms.rcms.spring_prototype.function_manager.parameter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ParameterDeserializer extends StdDeserializer<Parameter<?>> {

    private static final Logger logger = Logger.getLogger(ParameterDeserializer.class);

    private static final ObjectMapper mapper = new ObjectMapper();

    public ParameterDeserializer() {
        this(null);
    }

    public ParameterDeserializer(Class<?> cl) {
        super(cl);
    }

    @Override
    public Parameter<?> deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JsonProcessingException {
        JsonNode tree = p.getCodec().readTree(p);

        String name = tree.get("name").asText();
        JsonNode valueNode = tree.get("value");

        Object value = null;
        try {
            Class<?> valueClass = Class.forName(tree.get("valueType").asText());

            JavaType type;
            if (List.class.isAssignableFrom(valueClass)) {
                type = mapper.getTypeFactory()
                        .constructCollectionType(List.class, Parameter.class);
            } else if (Map.class.isAssignableFrom(valueClass)) {
                type = mapper.getTypeFactory()
                        .constructMapType(Map.class, String.class, Parameter.class);
            } else if (Set.class.isAssignableFrom(valueClass)) {
                type = mapper.getTypeFactory()
                        .constructCollectionType(Set.class, Parameter.class);
            } else {
                type = mapper.getTypeFactory().constructType(valueClass);
            }

            value = mapper.convertValue(valueNode, type);
        } catch (ClassNotFoundException ex) {
            logger.error("Simple parameter type not found while deserializing.", ex);
        }

        return new Parameter<>(name, value);
    }
}