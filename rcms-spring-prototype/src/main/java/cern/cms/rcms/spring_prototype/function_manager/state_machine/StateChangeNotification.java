package cern.cms.rcms.spring_prototype.function_manager.state_machine;

public class StateChangeNotification {

    private String identifier;
    private State state;

    public StateChangeNotification(String identifier, State state) {
        this.identifier = identifier;
        this.state = state;
    }

    private StateChangeNotification() {
    }

    public String getIdentifier() {
        return identifier;
    }

    public State getState() {
        return this.state;
    }

}
