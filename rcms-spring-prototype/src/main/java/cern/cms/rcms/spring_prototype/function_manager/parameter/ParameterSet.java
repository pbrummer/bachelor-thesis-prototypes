package cern.cms.rcms.spring_prototype.function_manager.parameter;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ParameterSet {

    @JsonProperty
    private Map<String, Parameter<?>> parameters = new ConcurrentHashMap<>();

    @JsonIgnore
    private Set<ParameterChangeListener> changeListeners = new HashSet<>();

    public ParameterSet() {}

    public Parameter<?> getParameter(String name) {
        return this.parameters.get(name);
    }

    public void setParameter(Parameter<?> parameter) {
        this.parameters.put(parameter.getName(), parameter);
        for (ParameterChangeListener changeListener : this.changeListeners) {
            changeListener.parameterChanged(parameter);
        }
    }

    public void addChangeListener(ParameterChangeListener changeListener) {
        this.changeListeners.add(changeListener);
    }

}
